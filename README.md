# kms1920
This repository was created as part of the KMS lecture in 2019/20. You can find the specific task of the lecture in the 
requirements_german.pdf file.

**Quick Start for File Server and Client:**  
1. `javac kms_exercise/client_server/*.java`
2. `java kms_exercise.client_server.Server` (starts server on port 4242)
3. `java kms_exercise.client_server.Client` (starts client)

**Quick Start for Peer-To-Peer Client:**
1. `javac kms_exercise/peer_to_peer/*.java`
2. `java kms_exercise.peer_to_peer.Main` (starts first client)
3. Open new terminal and execute again `java kms_exercise.peer_to_peer.Main` (starts second client)