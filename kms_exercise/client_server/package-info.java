/**
 * Exercise 1. of 2019/2020 "Betriebs- und Kommunikationssysteme" (Operating and Communication Systems) programming assignment at University of Leipzig.
 * Implements a TCP client and server.
 * @author Louis Fahrenholz & Stefan Rosenlund
 */
package kms_exercise.client_server;