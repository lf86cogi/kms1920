package kms_exercise.client_server;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.file.*;
import java.util.stream.*;
import java.nio.ByteBuffer;

/**
 * This class implements the TCP server.
 * @author Stefan Rosenlund
 */
public class Server {
	private ServerSocket socket;
	
	private String directory = ".";
    
	/**
	 * Listens in a loop for client requests, handles the requests of client and
	 * closes the connection when client closes socket
	 */
	public static void main(String[] args) {
		String directory = ".";
		int port = 4242;
		
		if (args.length >= 1) {
			try {
				port = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				System.err.println("Port must be an integer.");
				System.exit(1);
			}
			if(args.length >= 2) {
				directory = args[1].replaceAll("/+$", "");
				File file = new File(directory);
				if(!file.isDirectory()) {
					System.err.println("Not a directory: '" + directory + "'");
					System.exit(1);
				}
            }
		}
            
        Server server = new Server(port, directory);

		while (true) {
			Socket client = server.waitforRequest();

			while (true) {
				String read = server.handleRequest(client);
				if (read == null) {
					break;
				}
				server.handleResponse(client, read);
			}

			try {
				client.close();
			} catch (IOException e) {
				System.err.println("Could not close client socket!");
			}
		}
	}

	/**
	 * Constructor
	 * 
	 * @param port server port
	 */
	public Server(int port, String directory) {
		try {
            this.directory = directory;
            this.socket = new ServerSocket(port);
		} catch (IOException e) {
			System.err.println("Could not open server Socket!");
		}
	}

	/**
	 * listens for client requests
	 * 
	 * @return socket for communicating with client
	 */
	public Socket waitforRequest() {
		// configure server socket to throw exception after 60 seconds with no request
		try {
			this.socket.setSoTimeout(60000);
		} catch (SocketException e) {
			System.err.println("Could not set socket timeout!");
		}

		Socket client = null;
		try {
			client = this.socket.accept();
			System.out.println("Client accepted");
		} catch (SocketTimeoutException t) {
			try {
				this.socket.close();
			} catch (IOException e) {
				System.err.println("Could not close server socket!");
			}
			System.out.println("Server shut down");
			System.exit(0);
		} catch (IOException e) {
			System.err.println("Could not open clientSocket!");
		}

		return client;
	}

	/**
	 * reads client request
	 * 
	 * @return Command of client
	 */
	public String handleRequest(Socket client) {
		String read = null;
		try {
			InputStream stream = client.getInputStream();

			byte[] len = new byte[2];
			if(!(stream.read(len) == 2)) throw new IOException("Malformed packet!");
			short length = ByteBuffer.wrap(len).getShort();
			byte[] command = stream.readNBytes(length);
			read = new String(command);
		} catch (IOException e) {
			System.out.println("Disconnected from client!");
		}

		return read;
	}

	/**
	 * retrieves requested data and sends them to client
	 */
	public void handleResponse(Socket client, String read) {
		try {
			OutputStream stream = client.getOutputStream();

			byte[] result = new byte[0];
			byte[] success = new byte[1];
			success[0] = (byte) 0;
			if (read.equals("LIST")) {
				result = this.list().getBytes();
				success[0] = (byte) 1;
			} else if (read.matches("^GET .+")) {
				Path path = Paths.get(this.directory + System.getProperty("file.separator") + read.substring(4));
				if(Files.exists(path)){
					result = this.getFileContent(path);
					if(result == null) {
						result = "An error occured while reading the file.".getBytes();
					} else {
						success[0] = (byte) 1;
					}
				}else{
					String message = "File not found!";
					result = message.getBytes();
				}
			} else {
				String message = "Not supported command!";
				result = message.getBytes();
			}
			
			if(result.length + 1 > Short.MAX_VALUE) {
				result = ("Error: The requested data is more than " + (Short.MAX_VALUE - 1) + " bytes.").getBytes();
				success[0] = (byte) 0;
			}
			
			short length = (short) (result.length + 1);
			ByteBuffer buffer = ByteBuffer.allocate(length + 2);
			buffer.putShort(length);
			buffer.put(success);
			buffer.put(result);
			stream.write(buffer.array());
			stream.flush();
		} catch (IOException e) {
			System.err.println("Error during sending of response!");
		}
	}

	/**
	 * @return String with filenames of current directory and subdirectories
	 */
	public String list() {
		String files = "";
		try (Stream<Path> walk = Files.walk(Paths.get(this.directory))) {
			// regular file:
			// https://unix.stackexchange.com/questions/99276/what-is-a-regular-file
			files = walk.filter(Files::isRegularFile)
                .map(x -> x.toString())
                .map(x -> x.substring(this.directory.length()))
                .reduce("", (x, y) -> x + "\n" + y);
		} catch (IOException e) {
			System.err.println("Error during walk through directory!");
		}
		return files;
	}

	/**
	 * @param path path of the file requested
	 * @return byte array with file content
	 */
	public byte[] getFileContent(Path path) {
		byte[] fileContent = null;
		try {
			fileContent = Files.readAllBytes(path);
		} catch (IOException e) {
			System.err.println("Error during reading of file!");
		} catch (OutOfMemoryError e) {
			System.err.println("Requested file too large!");
		}
		return fileContent;
	}
}
