package kms_exercise.client_server;

import java.net.Socket;
import java.net.SocketTimeoutException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class implements the TCP client.
 * @author Louis Fahrenholz
 */
public class Client {

	/**
	 * The host (IP) address.
	 */
	private static InetAddress serverAddress;

	/**
	 * The host port. Port 4242 is the default.
	 */
	private static int serverPort = 4242;

	/**
	 * Timeout for read operations from socket.
	 */
	private static int timeout = 60000;

	/**
	 * The socket which is the interface to the server.
	 */
	private static Socket sock;

	/**
	 * OutputStream for writing/sending data to the server.
	 */
	private static OutputStream outStream;

	/**
	 * InputStream for reading/receiving data from the server.
	 */
	private static InputStream inStream;

	/**
	 * Scanner for reading user input.
	 */
	private static Scanner sc = new Scanner(System.in);

	/**
	 * Main method. Controls the general program sequence.
	 * 
	 * @param args The initial arguments.
	 */
	public static void main(String[] args) {
		if (args.length >= 1 && args[0].equals("--help")) {
			displayHelp();
			System.exit(0);
		}
		if (args.length >= 1) {
			handleArgs(args);
		} else {
			getArgs();
		}
		try {
			sock = new Socket(serverAddress, serverPort);
			sock.setSoTimeout(timeout);
			outStream = sock.getOutputStream();
			inStream = sock.getInputStream();
		} catch (IOException e) {
			System.err.println("Could not create socket: " + e.getMessage());
			System.exit(1);
		}
		System.out.println("Successfully connected to " + serverAddress + " on port " + serverPort);
		int status = interactiveCLI();
		try {
			sock.close();
		} catch (IOException e) {
			System.err.println("Could not close socket: " + e.getMessage());
			status = 1;
		}
		System.exit(status);
	}

	/**
	 * Main program loop. Manages sending and receiving data to and from the server.
	 */
	private static int interactiveCLI() {
		String input = sc.nextLine();
		input = input.trim();
		while (!input.equals("QUIT") && sock.isConnected()) {
			sendRequest(input);
			try {
				byte[] res = exceptResponse();
				handleResponse(res);
			} catch (SocketTimeoutException e) {
				System.err.println("The connection to the host timed out.");
				return 1;
			} catch (IOException e) {
				if (sock.isClosed()) {
					System.err.println("The connection to the host was was closed unexpectidly.");
					return 1;
				} else
					System.err.println("An error occurred while reading data from the host: " + e.getMessage());
			}
			input = sc.nextLine();
		}
		sendRequest(input);
		sc.close();
		return 0;
	}

	/**
	 * Evaluates the servers response and prints it to stdout or stderr
	 * respectively.
	 * 
	 * @param response The servers response as byte[] without the leading two bytes
	 *                 of the response.
	 * @throws IOException When the response doesn't match the protocol.
	 */
	private static void handleResponse(byte[] response) throws IOException {
		PrintStream sysOut = System.err;
		try {
			if (response[0] != 0)
				sysOut = System.out;
		} catch (IndexOutOfBoundsException e) {
			throw new IOException("Malformed packet");
		}
		sysOut.println(new String(Arrays.copyOfRange(response, 1, response.length)).stripTrailing());
	}

	/**
	 * Reads in the server's response.
	 * 
	 * @return The server's response without the leading two bytes that indicate the
	 *         response's length.
	 * @throws IOException When the response doesn't match the protocol or other
	 *                     transmission errors occur.
	 */
	private static byte[] exceptResponse() throws IOException, SocketTimeoutException {
		byte[] lenBuf = new byte[2];
		if (inStream.read(lenBuf) != 2)
			throw new IOException("Malformed packet");
		ByteBuffer wrapped = ByteBuffer.wrap(lenBuf);
		byte[] bodyBuf = new byte[wrapped.getShort(0)];
		if (inStream.read(bodyBuf) != wrapped.getShort(0))
			throw new IOException("Malformed packet");
		return bodyBuf;
	}

	/**
	 * Sends a request to the server.
	 * 
	 * @param request The request that should be sent to the server.
	 */
	private static void sendRequest(String request) {
		byte[] data = request.getBytes();
		ByteBuffer packet = ByteBuffer.allocate(data.length + 2);
		packet.putShort((short) data.length);
		packet.put(data);
		try {
			outStream.write(packet.array());
			outStream.flush();
		} catch (IOException e) {
			System.err.println("An error occurred while sending a command to the host: " + e.getMessage());
		}
	}

	/**
	 * Prints usage information.
	 */
	private static void displayHelp() {
		System.out.println("SYNOPSIS: 'java kms_exercise.client_server.Client [<host> [<port>]]'");
	}

	/**
	 * Handle and evaluate the initial arguments (required those were provided)
	 * passed to the program.
	 * 
	 * @param args The initial arguments.
	 */
	private static void handleArgs(String[] args) {
		try {
			serverAddress = InetAddress.getByName(args[0]);
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " + e.getMessage());
			System.exit(1);
		}
		if (args.length >= 2) {
			try {
				serverPort = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				System.err.println("Invalid format for port: " + e.getMessage());
				System.exit(1);
			}
		}
	}

	/**
	 * Get the initial arguments (if they were not provided) in an interactive
	 * manner from stdin.
	 */
	private static void getArgs() {
		System.out.println("Host address:");
		try {
			serverAddress = InetAddress.getByName(sc.nextLine());
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " + e.getMessage());
			System.exit(1);
		}
		System.out.println("Host port:");
		try {
			serverPort = sc.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Invalid format for port: " + e.getMessage());
			System.exit(1);
		}
		sc.nextLine();
	}
}
