package kms_exercise.peer_to_peer;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

/**
 * Container for client status values.
 * Provides methods for converting status information to a byte array and vice versa.
 * @author Louis Fahrenholz
 */
public class Status {
	private String location;
	private float temperature; // in °C
	private float humidity; // in %
	private Date timestamp;
	
	/**
	 * Constructs a Status object and initializes it's fields with random data.
	 * @param location Location string
	 */
	public Status(String location) {
		this.location = location;
		setRandom();
	}
	
	/**
	 * Constructs a Status object with the given data.
	 * Timestamp will be initialized with the current time.
	 * @param location Location string
	 * @param temperature
	 * @param humidity
	 */
	public Status(String location, float temperature, float humidity) {
		this.location = location;
		this.temperature = temperature;
		this.humidity = humidity;
		this.timestamp = new Date();
	}
	
	/**
	 * Constructs a Status object with the given values.
	 * @param location Location string
	 * @param temperature
	 * @param humidity
	 * @param timestamp
	 */
	public Status(String location, float temperature, float humidity, long timestamp) {
		this.location = location;
		this.temperature = temperature;
		this.humidity = humidity;
		this.timestamp = new Date(timestamp);
	}
	
	/**
	 * Returns a string representation of this object.
	 */
	@Override
	public String toString() {
		return this.location + ", " + this.temperature + "°C, " + this.humidity + "%, " + this.timestamp.toString() + ";";
	}
	
	/**
	 * Converts this object to a 64 byte long byte array.
	 * 6 bytes type (STATUS); 42 bytes location; 4 bytes temperature; 4 bytes humidity; 8 bytes timestamp;
	 * @return
	 */
	public byte[] toByteArr() {
		byte[] ret = new byte[64];
		ByteBuffer buf = ByteBuffer.wrap(ret);
		// 6 bytes packet type
		buf.put(new byte[] {'S' , 'T', 'A', 'T', 'U', 'S'});
		// 42 bytes location
		buf.put(this.location.getBytes());
		for(int i = buf.position(); i < 48; i++) buf.put((byte) 0);
		// 4 bytes temperature
		buf.putFloat(this.temperature);
		// 4 bytes humidity
		buf.putFloat(this.humidity);
		// 8 bytes timestamp
		buf.putLong(this.timestamp.getTime());
		return ret;
	}
	
	/**
	 * Converts a byte array to a Status object.
	 * 6 bytes type (STATUS); 42 bytes location; 4 bytes temperature; 4 bytes humidity; 8 bytes timestamp;
	 * @param arr 
	 * @return
	 */
	public static Status fromByteArr(byte[] arr) {
		if(arr.length != 64) throw new IllegalArgumentException("Input array must be exactly 64 bytes.");
		ByteBuffer buf = ByteBuffer.wrap(arr);
		byte[] type = new byte[6];
		buf.get(type);
		if(!Arrays.equals(type, new byte[] {'S' , 'T', 'A', 'T', 'U', 'S'})) throw new IllegalArgumentException("Incorrect packet type.");
		byte[] loc = new byte[42];
		buf.get(loc);
		int l = 42;
		while(loc[l - 1] == 0) l--;
		String location = new String(loc, 0, l);
		float temperature = buf.getFloat();
		float humidity = buf.getFloat();
		long timestamp = buf.getLong();
		return new Status(location, temperature, humidity, timestamp);
	}
	
	public String getLocation() {
		return this.location;
	}
	
	public Date getTimestamp() {
		return this.timestamp;
	}
	
	/**
	 * Returns true if this Status object is more recent than the given one.
	 * @param status
	 * @return
	 */
	public boolean isNewerThan(Status status) {
		return this.timestamp.after(status.getTimestamp());
	}

	/**
	 * Sets the Status to random values.
	 * Temperature [-20;40] rounded to first decimal; Humidity [0;100] rounded to first decimal;
	 */
	public void setRandom() {
		this.temperature = (float) Math.round((-20 + Math.random() * 40) * 10) / 10.0f;
		this.humidity = Math.round(Math.random() * 1000) / 10.0f;
		this.timestamp = new Date();
	}
}
