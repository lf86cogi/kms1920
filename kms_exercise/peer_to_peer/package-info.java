/**
 * Exercise 2. of 2019/2020 "Betriebs- und Kommunikationssysteme" (Operating and Communication Systems) programming assignment at University of Leipzig.
 * Implements a UDP client which is part of a P2P network.
 * @author Louis Fahrenholz & Stefan Rosenlund
 */
package kms_exercise.peer_to_peer;