package kms_exercise.peer_to_peer;

public class Main  {
    public static void main(String[] args){
        Client client = new Client();
        while(true) {
        	client.advertise();
        	client.receive();
        }
    }
}