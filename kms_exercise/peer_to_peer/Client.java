package kms_exercise.peer_to_peer;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.DatagramPacket;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Implements a client that sends and receives status information of other clients.
 * Multiple clients communicate connectionless via UDP in a peer2peer network.
 * @author Stefan Rosenlund
 */
public class Client {
	private static final int TIMEOUT = 1 * 1000;
	private static final int MIN_PORT = 50001;
	private static final int MAX_PORT = 50010;
	private int port;
	private InetAddress localhost;
	private DatagramSocket socket;
	private Status status;
	private Map<String, Status> neighbors;
	private Sensor sensor;
	private int nextPort;

	/**
	 * Constructs a new client.
	 * Looks for a free port in the given port range.
	 */
	public Client() {
		for (this.port = MIN_PORT; port <= MAX_PORT; port++) {
			try {
				this.socket = new DatagramSocket(this.port);
				this.nextPort = (this.port + 1 <= MAX_PORT) ? this.port + 1 : MIN_PORT;
				System.out.println("Running on port " + this.port);
				break;
			} catch (IOException e) {
				if (this.port == MAX_PORT) {
					System.err.println("No available port.");
					System.exit(1);
				}
			}
		}
		try {
			this.localhost = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			System.err.println("'localhost' could not be resolved to an address.");
			System.exit(1);
		}

		this.status = new Status(askForPlace());
		this.neighbors = new HashMap<>();
		this.neighbors.put(this.status.getLocation(), this.status);
		this.sensor = new Sensor(this.status);
		this.sensor.start();
	}

	/**
	 * Sends advertisement packets to other clients.
	 * Advertisement packets are 64 bytes long and consist of 6 type bytes (ADVERT) and the location of this client.
	 * @author Louis Fahrenholz
	 */
	public void advertise() {
		if (this.nextPort == this.port) this.nextPort = (this.nextPort + 1 <= MAX_PORT) ? this.nextPort + 1 : MIN_PORT;
		ByteBuffer buf = ByteBuffer.allocate(64);
		buf.put(new byte[] { 'A', 'D', 'V', 'E', 'R', 'T' });
		buf.put(this.status.getLocation().getBytes());
		for (int i = buf.position(); i < 64; i++) buf.put((byte) 0);
		DatagramPacket ad = new DatagramPacket(buf.array(), 64, this.localhost, this.nextPort);
		try {
			this.socket.send(ad);
		} catch (IOException e) {
			System.err.println("Could not send advertisment packet: " + e.getMessage());
		}
		this.nextPort = (this.nextPort + 1 <= MAX_PORT) ? this.nextPort + 1 : MIN_PORT;
	}

	/**
	 * Receives packets of other clients and handles them accordingly.
	 * Prints all clients statuses when a new status packet was received.
	 */
	public void receive() {
		try {
			this.socket.setSoTimeout(TIMEOUT);
		} catch (SocketException e) {
			System.err.println("Could net set timeout!");
		}

		byte[] receivedData = new byte[64];
		DatagramPacket packet = new DatagramPacket(receivedData, receivedData.length);

		try {
			this.socket.receive(packet);
		} catch (SocketTimeoutException e) {
			return;
		} catch (IOException e) {
			System.err.println("An error occured while trying to read data: " + e.getMessage());
		}

		ByteBuffer buf = ByteBuffer.wrap(receivedData);
		byte[] type = new byte[6];
		buf.get(type);

		// test if packet is advertisement packet (discover)
		if (Arrays.equals(type, new byte[] { 'A', 'D', 'V', 'E', 'R', 'T' })) {
			// find out location
			byte[] loc = new byte[42];
			buf.get(loc);
			int l = 42;
			while (loc[l - 1] == 0)
				l--;
			send(packet.getSocketAddress(), new String(loc, 0, l));
			return;
		}

		Status neighbor;
		try {
			neighbor = Status.fromByteArr(receivedData);
		} catch (IllegalArgumentException e) {
			System.err.println("Malformed packet: " + e.getMessage());
			return;
		}

		if (!this.neighbors.containsKey(neighbor.getLocation()) || neighbor.isNewerThan(this.neighbors.get(neighbor.getLocation()))) {
			this.neighbors.put(neighbor.getLocation(), neighbor);
			printStatuses();
		}
	}

	private void send(SocketAddress address, String remoteLocation) {
		for (Map.Entry<String, Status> neighbor : this.neighbors.entrySet()) {
			if (neighbor.getKey().equals(remoteLocation))
				continue;
			sendPacket(address, neighbor.getValue());
		}
	}

	/**
	 * Sends one status packet.
	 * @param address
	 * @param status
	 * @author Louis Fahrenholz
	 */
	private void sendPacket(SocketAddress address, Status status) {
		DatagramPacket packet = new DatagramPacket(status.toByteArr(), 64, address);
		try {
			this.socket.send(packet);
		} catch (IOException e) {
			System.err.println("Could not send status packet: " + e.getMessage());
		}
	}

	/**
	 * Asks user for client location.
	 * 
	 * @return Client location
	 */
	@SuppressWarnings("resource")
	private static String askForPlace() {
		Scanner s = new Scanner(System.in);

		System.out.println("Location of the client:");
		String ort = s.nextLine();
		ort = ort.trim();
		ort = ort.toLowerCase();
		if(ort.getBytes().length > 42) {
			System.err.println("Location name must not be longer than 42 bytes.");
			return askForPlace();
		}
		s.close();

		return ort;
	}

	/**
	 * Prints statuses of all known clients.
	 * @author Louis Fahrenholz
	 */
	private void printStatuses() {
		for (Status status : neighbors.values()) System.out.println(status.toString());
		System.out.println();
	}
}