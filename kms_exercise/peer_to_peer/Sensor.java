package kms_exercise.peer_to_peer;

/**
 * Class runs in a separate thread and sets the Status' data to random values every 10 seconds.
 * @author Louis Fahrenholz
 */
public class Sensor extends Thread {

	private Status status;
	
	public Sensor(Status status) {
		this.status = status;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(10 * 1000);
			} catch (InterruptedException e) {
				continue;
			}
			status.setRandom();
		}
	}
}
