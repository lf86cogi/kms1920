/**
 * 2019/2020 "Betriebs- und Kommunikationssysteme" (Operating and Communication Systems) programming assignment at University of Leipzig.
 * Implements a TCP client/server application and a UDP P2P application.
 * @author Louis Fahrenholz & Stefan Rosenlund
 */
package kms_exercise;